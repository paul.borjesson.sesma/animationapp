package com.example.animationapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    AnimationDrawable animation;
    Button button;
    Button button2;
    Button button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);
        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);

        animation = new AnimationDrawable();

        animation = iterateImage("baymax", 1, 30, 30, true);
        animation.setOneShot(false);
        imageView.setImageDrawable(animation);
        animation.start();

        button.setOnClickListener(view -> {
            animation = new AnimationDrawable();
            animation = iterateImage("rocket", 17, 167, 20, true);
            animation.setOneShot(true);
            imageView.setImageDrawable(animation);
            animation.start();
            button.setEnabled(false);
            button.setAlpha(0);
            button2.setEnabled(true);
            button2.setAlpha(1);
        });

        button2.setOnClickListener(view -> {
            animation = new AnimationDrawable();
            animation = iterateImage("spaceship", 1, 225, 40, false);
            animation.setOneShot(true);
            imageView.setImageDrawable(animation);
            animation.start();
            button2.setEnabled(false);
            button2.setAlpha(0);
            button3.setEnabled(true);
            button3.setAlpha(1);
        });

        button3.setOnClickListener(view -> {
            animation = new AnimationDrawable();
            animation = iterateImage("baymax", 1, 30, 30, true);
            animation.setOneShot(false);
            imageView.setImageDrawable(animation);
            animation.start();
            button3.setEnabled(false);
            button3.setAlpha(0);
            button.setEnabled(true);
            button.setAlpha(1);
        });

    }

    private AnimationDrawable iterateImage(String name, int start, int total, int duration, boolean format) {
        for (int i = start; i <= total; i++) {
            animation.addFrame(ResourcesCompat.getDrawable(
                    getResources(), getResources().getIdentifier(
                            name + formatNum(i, format), "drawable", getPackageName()),
                    null
                    ),
            duration);
        }
        return animation;
    }

    private String formatNum(int i, boolean format) {
        String ret;
        if (format) {
            if (i < 10) {
                ret = "0" + i;
            } else {
                ret = String.valueOf(i);
            }
        } else {
            ret = String.valueOf(i);
        }
        return ret;
    }
}